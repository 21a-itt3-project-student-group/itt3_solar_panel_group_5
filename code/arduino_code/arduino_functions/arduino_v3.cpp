#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BNO055.h>
#include <utility/imumaths.h>
#define BNO055_SAMPLERATE_DELAY_MS (100)
 
Adafruit_BNO055 myIMU = Adafruit_BNO055();
String received_data= "";
bool is_moving = true;

// acceleration check variables
bool mech_fail = false;
float XAcc;
float YAcc;
float ZAcc;


// defining the max angels the solar panel can turn
const int MaxPosX = 40.0;
const int MaxPosY = 45.0;
const int MaxPosZ = 40.0;
const int MaxNegX = -40.0;
const int MaxNegY = -45.0;
const int MaxNegZ = -40.0;
float XValue;
float YValue;
float ZValue;


// D7
const int pwm0 = 7; 
// D6
const int in_1 = 6; 
// D5
const int in_2 = 5; 

// D8
const int in_4 = 8;
// D9
const int in_3 = 9;
// D10
const int pwm1 = 10;

void setup() {
  // serial setup
    Serial.begin(115200);
    Serial.print("arduino program starting");

  // pin setup
    pinMode(pwm0,OUTPUT); 
    pinMode(in_1,OUTPUT); 
    pinMode(in_2,OUTPUT);
    pinMode(pwm1,OUTPUT); 
    pinMode(in_3,OUTPUT); 
    pinMode(in_4,OUTPUT);
    Serial.print("pin setup complete");
    
  // BNO055 setup
    myIMU.begin();
    int8_t temp=myIMU.getTemp();
    myIMU.setExtCrystalUse(true);
    Serial.print("BNO055 setup complete");
} 

void loop(){
  read_data();
  move_actuator();
  collision_check();
  angel_check();
}


String read_data(){
     if (Serial.available() > 0) {
     received_data = Serial.readString();
     Serial.print("instructions received");
     return received_data;
     }
}

bool move_actuator() {
    received_data = "";
    analogWrite(pwm0,255);
    analogWrite(pwm1,255);
    if (received_data == "m1"){
          digitalWrite(in_1,HIGH); 
          digitalWrite(in_2,LOW);
          digitalWrite(in_3,HIGH); 
          digitalWrite(in_4,HIGH); 
          is_moving = false;
          delay(1000);

      }

    else if (received_data == "m2"){
          digitalWrite(in_1,HIGH); 
          digitalWrite(in_2,HIGH);
          digitalWrite(in_3,HIGH); 
          digitalWrite(in_4,LOW);
          is_moving = false;
          delay(1000);
      
      }

   else if (received_data == "m3"){
          digitalWrite(in_1,LOW); 
          digitalWrite(in_2,HIGH);
          digitalWrite(in_3,HIGH); 
          digitalWrite(in_4,HIGH);  
          is_moving = false;
          delay(1000);

      }

   else if (received_data == "m4"){
          digitalWrite(in_1,HIGH); 
          digitalWrite(in_2,HIGH);
          digitalWrite(in_3,LOW); 
          digitalWrite(in_4,HIGH);
          is_moving = false;
          delay(1000);

      }
      
   else {
        digitalWrite(in_1,HIGH); 
        digitalWrite(in_2,HIGH);
        digitalWrite(in_3,HIGH); 
        digitalWrite(in_4,HIGH);
        is_moving = true;  
        Serial.println("Waiting for instructions");
        delay(1000);

   }
   return is_moving; 
}


bool collision_check(){
  imu::Vector<3> acc =myIMU.getVector(Adafruit_BNO055::VECTOR_ACCELEROMETER);
  XAcc = acc[0];
  YAcc = acc[1];
  YAcc = acc[2];
    if (is_moving && XAcc <= 0.1 || YAcc <= 0.1 || ZAcc <= 0.1){
      Serial.print("should be moving but not");
      mech_fail = true;
      return mech_fail;
      
    }

  else{
    Serial.print("all good");
    mech_fail = false;
    return mech_fail;
 }
}


bool angel_check(){
  imu::Vector<3> mag =myIMU.getVector(Adafruit_BNO055::VECTOR_MAGNETOMETER);
  XValue = mag[0];
  YValue = mag[1];
  YValue = mag[2];
  
  if (XValue > MaxPosX || XValue < MaxNegX){
    Serial.print("Out of bounds on X axis");
    Serial.print("X-value:");
  }
 else if (YValue > MaxPosY || YValue < MaxNegY){
    Serial.print("Out of bounds on Y axis");
    Serial.print("X-value:");
  }
 else if (ZValue > MaxPosZ || ZValue < MaxNegZ){
    Serial.print("Out of bounds on Z axis");
    Serial.print("X-value:");
  }
 else{
  Serial.print("X-value:");
  Serial.print(mag.x());
  Serial.print(", Y-value:");
  Serial.print(mag.y());
  Serial.print(", Z-value:");
  Serial.println(mag.z());
 }
  
}



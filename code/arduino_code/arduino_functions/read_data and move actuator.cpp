String received_data= "";
bool is_moving = false;

// D7
const int pwm0 = 7; 
// D6
const int in_1 = 6; 
// D5
const int in_2 = 5; 

// A5
const int in_3 = 19;
// A6
const int in_4 = 20;
// D10
const int pwm1 = 10;

void setup() {
  // serial setup
    Serial.begin(115200);

  // pin setup
    pinMode(pwm0,OUTPUT); 
    pinMode(in_1,OUTPUT); 
    pinMode(in_2,OUTPUT);
    pinMode(pwm1,OUTPUT); 
    pinMode(in_3,OUTPUT); 
    pinMode(in_4,OUTPUT);  
} 

void loop(){
  received_data = read_data();
  move_actuator();
}


String read_data(){
     if (Serial.available() > 0) {
     received_data = Serial.readString();
     Serial.print("instructions received");
     return received_data;
     }
}

bool move_actuator() {
    if (received_data == "m1"){
          digitalWrite(in_1,HIGH); 
          digitalWrite(in_2,LOW);
          digitalWrite(in_3,HIGH); 
          digitalWrite(in_4,HIGH); 
          analogWrite(pwm0,255);
          is_moving = true;
          delay(1000);
      }

    else if (received_data == "m2"){
          digitalWrite(in_1,HIGH); 
          digitalWrite(in_2,HIGH);
          digitalWrite(in_3,HIGH); 
          digitalWrite(in_4,LOW);
          analogWrite(pwm1,255);
          is_moving = true;
          delay(1000);       
      }

   else if (received_data == "m3"){
          digitalWrite(in_1,LOW); 
          digitalWrite(in_2,HIGH);
          digitalWrite(in_3,HIGH); 
          digitalWrite(in_4,HIGH);  
          analogWrite(pwm0,255);
          is_moving = true;
          delay(1000);
      }

   else if (received_data == "m4"){
          digitalWrite(in_1,HIGH); 
          digitalWrite(in_2,HIGH);
          digitalWrite(in_3,LOW); 
          digitalWrite(in_4,HIGH);
          analogWrite(pwm1,255);
          is_moving = true;
          delay(1000); 
      }
      
   else {
        digitalWrite(in_1,HIGH); 
        digitalWrite(in_2,HIGH);
        digitalWrite(in_3,HIGH); 
        digitalWrite(in_4,HIGH);
        is_moving = false;  
        Serial.println("Waiting for instructions");
   }
   return is_moving; 
}
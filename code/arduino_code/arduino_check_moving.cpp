#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BNO055.h>
#include <utility/imumaths.h>


 
#define BNO055_SAMPLERATE_DELAY_MS (100)
 
Adafruit_BNO055 myIMU = Adafruit_BNO055();


bool IsMoving = true;
float XAcc;
float YAcc;
float ZAcc;


 
void setup() {
  Serial.begin(115200);
  myIMU.begin();
  delay(1000);
  int8_t temp=myIMU.getTemp();
  myIMU.setExtCrystalUse(true);
}

void loop() {
  delay(2000);
  imu::Vector<3> acc =myIMU.getVector(Adafruit_BNO055::VECTOR_ACCELEROMETER);
  XAcc = acc[0];
  YAcc = acc[1];
  YAcc = acc[2];
    if (XAcc <= 0.1 || YAcc <= 0.1 || ZAcc <= 0.1 && IsMoving){
      Serial.print("should be moving but not");
    }

  else{
    Serial.print("all good");
 }
}


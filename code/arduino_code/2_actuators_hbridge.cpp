// D7
const int pwm = 7; 
// D6
const int in_1 = 6; 
// D5
const int in_2 = 5; 


// D8
const int in_3 = 8;
// D9
const int in_4 = 9;
// D10
const int pwm1 = 10;

void setup() { 
    pinMode(pwm,OUTPUT); 
    pinMode(in_1,OUTPUT); 
    pinMode(in_2,OUTPUT);
    pinMode(pwm1,OUTPUT); 
    pinMode(in_3,OUTPUT); 
    pinMode(in_4,OUTPUT);  
} 


void loop() { 
    //for clock wise motion 
    digitalWrite(in_1,HIGH); 
    digitalWrite(in_2,LOW); 
    analogWrite(pwm,255);
    digitalWrite(in_3,HIGH); 
    digitalWrite(in_4,LOW); 
    analogWrite(pwm1,255);  
    //delay for 3 sec Clock wise motion 
    delay(1000);

    //for break 
    digitalWrite(in_1,HIGH); 
    digitalWrite(in_2,HIGH);
    digitalWrite(in_3,HIGH); 
    digitalWrite(in_4,HIGH);  
    delay(200); 

    //for anticlock wise 
    digitalWrite(in_1,LOW); 
    digitalWrite(in_2,HIGH);
    digitalWrite(in_3,LOW); 
    digitalWrite(in_4,HIGH);  
    delay(3000);

    //for break 
    digitalWrite(in_1,HIGH); 
    digitalWrite(in_2,HIGH);
    digitalWrite(in_3,HIGH); 
    digitalWrite(in_4,HIGH);  
    delay(200); 
}
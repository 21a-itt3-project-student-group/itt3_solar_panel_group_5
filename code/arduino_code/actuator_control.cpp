int in1 = 10;
int in2 = 11;

void setup() {
  pinMode(in1, OUTPUT);
  pinMode(in2, OUTPUT);
}

void loop() {
  //makes the actuator retract, reverse LOW/HIGH for extend
  digitalWrite(in1, LOW);
  digitalWrite(in2, HIGH);
  delay(10000);
}
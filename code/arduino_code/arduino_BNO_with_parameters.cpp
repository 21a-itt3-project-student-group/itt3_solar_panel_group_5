#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BNO055.h>
#include <utility/imumaths.h>
 
#define BNO055_SAMPLERATE_DELAY_MS (100)
 
Adafruit_BNO055 myIMU = Adafruit_BNO055();

// defining the max angels the solar panel can turn
const int MaxPosX = 40.0;
const int MaxPosY = 45.0;
const int MaxPosZ = 40.0;
const int MaxNegX = -40.0;
const int MaxNegY = -45.0;
const int MaxNegZ = -40.0;
float XValue;
float YValue;
float ZValue;
bool Checkangel(XValue);

 
void setup() {
  Serial.begin(115200);
  myIMU.begin();
  delay(1000);
  int8_t temp=myIMU.getTemp();
  myIMU.setExtCrystalUse(true);
}


void loop() {
  imu::Vector<3> mag =myIMU.getVector(Adafruit_BNO055::VECTOR_MAGNETOMETER);
  XValue = mag[0];
  YValue = mag[1];
  YValue = mag[2];
  
  if (XValue > MaxPosX || XValue < MaxNegX){
    Serial.print("Out of bounds on X axis");
    Serial.print("X-value:");
  }
 else if (YValue > MaxPosY || YValue < MaxNegY){
    Serial.print("Out of bounds on Y axis");
    Serial.print("X-value:");
  }
 else if (ZValue > MaxPosZ || ZValue < MaxNegZ){
    Serial.print("Out of bounds on Z axis");
    Serial.print("X-value:");
  }
 else{
  Serial.print("X-value:");
  Serial.print(mag.x());
  Serial.print(", Y-value:");
  Serial.print(mag.y());
  Serial.print(", Z-value:");
  Serial.println(mag.z());
 }
delay(1000);
 
}

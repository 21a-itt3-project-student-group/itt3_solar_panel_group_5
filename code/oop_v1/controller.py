import serial
from platform import system
from serial_receive import ReceiveClass
from serial_send import SendClass


class ControllerClass:
    def __init__(self):
        self.os_setup()
        self.move_command = None
        self.receive = ReceiveClass(self.serial_connection)
        self.send = SendClass(self.serial_connection, self.move_command)

    def os_setup(self):
        """
        this method determines the OS of the machine and returns the self.serial_connection object

        """
        os = system()
        if os == 'Windows':
            print('this is a Windows machine')
            self.serial_connection = serial.Serial("COM4", 11500, timeout=0)
            self.serial_connection.set_buffer_size(rx_size = 12800, tx_size = 12800)
            print('device open')
            print("---------------------- \nenter 1 to move actuator 1 up \n----------------------\nenter 2 to move actuator 1 down\n---------------------- \nenter 3 to move actuator 2 up \n----------------------\nenter 4 to move actuator 2 down\n ")
        elif os == 'Linux':
            print('this is a Linux machine')
            self.serial_connection = serial.Serial("/dev/ttyUSB0", 11500, timeout=0)
            self.serial_connection.set_buffer_size(rx_size = 12800, tx_size = 12800)
            print('device open')
            print("---------------------- \nenter 1 to move actuator 1 up \n----------------------\nenter 2 to move actuator 1 down\n---------------------- \nenter 3 to move actuator 2 up \n----------------------\nenter 4 to move actuator 2 down\n ")
        else:
            print('Could not determine OS, shutting off')
            quit()

    def command(self):
        if self.user_input == "1":
            self.move_command = "m1"
        elif self.user_input == "2":
            self.move_command = "m2"
        elif self.user_input == "3":
            self.move_command = "m3"
        elif self.user_input == "4":
            self.move_command = "m4"
        else:
            pass


    def port_close(self):
        if self.serial_connection.is_open:
            self.serial_connection.close()

    def start_send(self):
        self.send.send_data(self.move_command)

    def start_receive(self):
        self.receive.packet_receive()

    def loop(self):
        try:
            while True:
                self.user_input  = input('>')
                self.command()
                self.start_send()
                self.start_receive()
        except KeyboardInterrupt:
            print('Interrupted')
            self.port_close()
            exit(0)

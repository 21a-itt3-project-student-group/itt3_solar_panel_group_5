import serial
from requests import get, ConnectionError


def serial_setup():
	"""
	takes in none, innitializes a serial connection variable from the port and the baudrate
	with a 5 sec timeout
	returns the serial connection

	"""
	#port = "/dev/ttyUSB0"
	port = "COM4"
	baudrate = 115200
	timeout = 1
	parity = serial.PARITY_NONE,
	stopbits = serial.STOPBITS_ONE,
	bytesize = serial.EIGHTBITS
	serial_connection = serial.Serial(port, baudrate)
	return serial_connection

def connectivity_check():
	try:
		response = get("https://www.google.com")
	except ConnectionError:
		print("no internet connection found")
		internet = bool(False)
		exit(1)


def packet_receive(serial_connection):
	"""
	takes in the serial connection object
	returns data gathered from the serial port
	should be used in a loop maybe also maybe .readline should be replaced with .read instead

	"""
	data = serial_connection.readline()
	clean_data = str(data, "utf-8")
	print(clean_data)
	if clean_data != None:
		return clean_data

def send_data(serial_connection):
	msg_data = input('enter a message to send to arduino:')
	byte_msg = msg_data.encode('utf-8')
	serial_connection.write(byte_msg)


def splitter(clean_data):
	"""
	takes in a string, converts it to a string with utf-8 decoding 
	splits the data on "," and returns a touple containing the x, y and z value

	"""
	split_packet=clean_data.split(",")
	x=float(split_packet[0])
	y=float(split_packet[1])
	z=float(split_packet[2])
	return x, y, z



def request_angel(serial_connection):
	"""
	function sending a specified msg which the arduino responds to by sending angel data

	"""
	request_msg = 'reqang'
	byte_req_msg = request_msg.encode('utf-8')
	serial_connection.write(byte_req_msg)
	print("requested angel measurements")



def port_close(serial_connection):
	if serial_connection.is_open:
		serial_connection.close()



def loop():
	connectivity_check()
	serial_connection = serial_setup()
	try:
		while True:
			send_data(serial_connection)
			clean_data = packet_receive(serial_connection)
			x, y, z = splitter(clean_data)
			print(x, y, z)
	except KeyboardInterrupt:
		print ('Interrupted')
		port_close()
		exit(0)


loop()


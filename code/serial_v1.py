import serial

def serial_setup():
	"""
	takes in none, innitializes a serial connection variable from the port and the baudrate
	with a 5 sec timeout
	returns the serial connection

	"""
	#port = "/dev/ttyUSB0"
	port = "COM4"
	baudrate = 115200
	serial_connection = serial.Serial(port, baudrate, timeout=0.050)
	serial_connection.set_buffer_size(rx_size = 12800, tx_size = 12800)
	return serial_connection


def send_data(serial_connection):
	msg_data = input('enter a message to send to arduino')
	byte_msg = msg_data.encode('utf-8')
	serial_connection.write(byte_msg)



serial_connection = serial_setup()


while True:
	send_data(serial_connection)
	try:
		if serial_connection.in_waiting > 0:
			data = serial_connection.readline(50)
			clean_data = str(data, "utf-8")
			print(clean_data)
		else:
			continue
	except UnicodeDecodeError:
		continue


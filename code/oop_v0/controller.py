import serial
from platform import system
from serial_receive import ReceiveClass
from serial_send import SendClass


class ControllerClass:
    def __init__(self):
        self.serial_connection = None
        self.os_setup()
        self.receive = ReceiveClass(self.serial_connection)
        self.send = SendClass(self.serial_connection)

    def os_setup(self):
        """
        this method determines the OS of the machine and returns the self.serial_connection object

        """
        os = system()
        timeout = 1
        parity = serial.PARITY_NONE,
        stopbits = serial.STOPBITS_ONE,
        bytesize = serial.EIGHTBITS
        if os == 'Windows':
            print('this is a Windows machine')
            self.serial_connection = serial.Serial("COM4", 11500)
            print('device open')
        elif os == 'Linux':
            print('this is a Linux machine')
            self.serial_connection = serial.Serial("/dev/ttyUSB0", 11500)
        else:
            print('Could not determine OS, shutting off')
            quit()

    def port_close(self):
        if self.serial_connection.is_open:
            self.serial_connection.close()

    def start_send(self):
        self.send.send_data()

    def start_receive(self):
        self.receive.packet_receive()

    def loop(self):
        try:
            while True:
                self.start_send()
                self.start_receive()
        except KeyboardInterrupt:
            print('Interrupted')
            self.port_close()
            exit(0)

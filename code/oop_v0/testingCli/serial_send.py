class SendClass:
    def __init__(self, serial_connection):
        self.serial_connection = serial_connection

    def send_data(self):
        byte_msg = self.msg_data.encode('utf-8')
        self.serial_connection.write(byte_msg)

    def request_angel(self):
        """
        method sending a specified msg which the arduino responds to by sending angel data

        """
        request_msg = 'reqang'
        byte_req_msg = request_msg.encode('utf-8')
        self.serial_connection.write(byte_req_msg)
        print("requested angel measurements")

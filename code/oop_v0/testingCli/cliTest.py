#!/usr/bin/env python3

import time
from serial_send import SendClass
from simple_term_menu import TerminalMenu

class commandLineInterfaceClass:
    def __init__(self):
        self.send = SendClass()
        self.cli_setup()

    def cli_setup(self):
        main_menu_title = "  Solar panel group number 3\n"
        main_menu_items = ["Manual movement", "Automatic movement", "About", "Quit"]
        main_menu_cursor = "> "
        main_menu_cursor_style = ("fg_red", "bold")
        main_menu_style = ("bg_red", "fg_yellow")
        self.main_menu_exit = False

        self.main_menu = TerminalMenu(
            menu_entries=main_menu_items,
            title=main_menu_title,
            menu_cursor=main_menu_cursor,
            menu_cursor_style=main_menu_cursor_style,
            menu_highlight_style=main_menu_style,
            cycle_cursor=True,
            clear_screen=True,
        )

        edit_menu_title = "  Manual movement menu\n"
        edit_menu_items = ["Move up", "Move down", "Move right", "Move left", "Back to Main Menu"]
        self.edit_menu_back = False
        self.edit_menu = TerminalMenu(
            edit_menu_items,
            title=edit_menu_title,
            menu_cursor=main_menu_cursor,
            menu_cursor_style=main_menu_cursor_style,
            menu_highlight_style=main_menu_style,
            cycle_cursor=True,
            clear_screen=True,
        )
    def cli_work(self):
        while not self.main_menu_exit:
            main_sel = self.main_menu.show()

            if main_sel == 0:
                while not self.edit_menu_back:
                    edit_sel = self.edit_menu.show()
                    if edit_sel == 0:
                        print("Moving up the solar panel for 1 second")
                        self.msg_data = "m1"
                        self.send.send_data()
                        time.sleep(5)
                    elif edit_sel == 1:
                        print("Move down the solar panel for 1 second")
                        self.msg_data = "m2"
                        self.send.send_data()
                        time.sleep(5)
                    elif edit_sel == 2:
                        print("Move right the solar panel for 1 second")
                        self.msg_data = "m3"
                        self.send.send_data()
                        time.sleep(5)
                    elif edit_sel == 3:
                        print("Move left the solar panel for 1 second")
                        self.msg_data = "m4"
                        self.send.send_data()
                        time.sleep(5)
                    elif edit_sel == 4:
                        self.edit_menu_back = True
                        print("Back Selected")
                self.edit_menu_back = False
            elif main_sel == 1:
                print("Not implemented yet, you will be sent back to the main menu in 3 seconds...")
                time.sleep(3)
            elif main_sel == 2:
                print("This project is developed by Solar panel team number 3\nGroup members:\nSimon Bjerre\nCasper Nielsen\nVladimir Rotaru\n\nReturning to main manu in 5 seconds...")
                time.sleep(5)
            elif main_sel == 3:
                self.main_menu_exit = True
                print("Quit Selected")
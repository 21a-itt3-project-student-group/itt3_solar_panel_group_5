from controller import ControllerClass


def main():
    CC = ControllerClass()
    CC.loop()


if __name__ == "__main__":
    main()

Group 3 Solar Panel Project
<br>

[[_TOC_]]

## :file_folder: documentation
### Project plan
[project_plan]
<br>
### Notes
[notes]
<br>


## :computer: code
### Python code 
[python_code]
<br>
### C++ code
[arduino_code]
<br>

## :books: Extra material
### images
[images]
<br>

### Datasheets
[datasheets]
<br>

### Wiring_diagrams
[wiring_diagram]
<br>

### Issue templates
[template]
<br>

## :email: members/contact
[Simon] 
<br>
[Casper]
<br>
[Vladimir]

## :wrench: tools
  <code><img width="10%" src="https://www.vectorlogo.zone/logos/raspberrypi/raspberrypi-ar21.svg"></code>
  <code><img width="10%" src="https://www.vectorlogo.zone/logos/linux/linux-ar21.svg"></code>
  <code><img width="10%" src="https://www.vectorlogo.zone/logos/python/python-ar21.svg"></code>
  <br />
  <code><img width="10%" src="https://www.vectorlogo.zone/logos/gitlab/gitlab-ar21.svg"></code>
  <code><img width="10%" src="https://www.vectorlogo.zone/logos/git-scm/git-scm-ar21.svg"></code>
  <code><img width="10%" src="https://www.vectorlogo.zone/logos/arduino/arduino-ar21.svg"></code>
  <br />
  <code><img width="10%" src="https://www.vectorlogo.zone/logos/gnu_bash/gnu_bash-ar21.svg"></code>
  <code><img width="10%" src="https://www.vectorlogo.zone/logos/yaml/yaml-ar21.svg"></code>
  <code><img width="10%" src="https://www.vectorlogo.zone/logos/json/json-ar21.svg"></code>


[wiring_diagram]: https://gitlab.com/21a-itt3-project-student-group/itt3_solar_panel_group_5/-/tree/main/images/wiring_diagrams
[datasheets]: https://gitlab.com/21a-itt3-project-student-group/itt3_solar_panel_group_5/-/tree/main/datasheets
[pages]:tbd
[brainstorm]: https://gitlab.com/21a-itt3-project-student-group/itt3_solar_panel_group_5/-/blob/main/documentation/Brainstorming%2014.09.2021.md
[notes]: https://gitlab.com/21a-itt3-project-student-group/itt3_solar_panel_group_5/-/tree/main/Notes
[project_plan]: https://gitlab.com/21a-itt3-project-student-group/itt3_solar_panel_group_5/-/tree/main/documentation/Project_plan
[python_code]: https://gitlab.com/21a-itt3-project-student-group/itt3_solar_panel_group_5/-/tree/main/code
[arduino_code]: https://gitlab.com/21a-itt3-project-student-group/itt3_solar_panel_group_5/-/tree/main/code/arduino_code
[template]: https://gitlab.com/21a-itt3-project-student-group/itt3_solar_panel_group_5/-/tree/main/.gitlab/issue_templates 
[images]:https://gitlab.com/21a-itt3-project-student-group/itt3_solar_panel_group_5/-/tree/main/images
[Simon]: https://gitlab.com/SimonBjerre
[Vladimir]: https://gitlab.com/senker
[Casper]: https://gitlab.com/thenillernielsen





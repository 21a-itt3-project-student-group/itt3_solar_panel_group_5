# Title
"include date/time" 

## Summary
"A short summary of the task"

## What needs to be done

"checklist for subtasks"

- [ ] 
- [ ] 
- [ ] 
- [ ] 
- [ ] 

# Output

"What is the expected output ?"

## Responsible
"Who is the main person responsible for this task, include tag if needed"


## Link to documentation related to the task
"Insert link(s)"

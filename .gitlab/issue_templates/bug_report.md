

# Title
"title please include date/time

## Expected Behavior
"what should happen "

## Current Behavior
"Tell us what happens instead of the expected behavior"

## Possible Solution
"Not obligatory, but suggest a fix/reason for the bug"

## Steps to Reproduce
"Explain what led up to the bug"
1.
2.
3.
4.

## Further context
"Add extra context if needed"

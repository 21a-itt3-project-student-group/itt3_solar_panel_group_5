# Brainstorming using Group passing method

## Intro
Working with this project a general idea was set for the project , thus limiting the idea generation in relation to which project to undertake instead the brainstorm is used to expand the initial vision provided by the stakeholders.
Brainstorming activated a lot of ideas in our group which would start from identifying potential features which would benefit the user experience and allow for our group to provide our own personal spin on the task chosen.



## What system will we be building?


### 1.(Casper)
We want a sensor that measures the 
angle so the system can know where it is in 3d space, and that way it knows when it has turned too much.
* Hardcoding parameters and sending error messages once parameters met
* Using a sensor to determine the angle would require more implementation inside the system such as: taking care so that the system can detect when the sensor is inactive.


We want a system that controls the hardware via arduino mini because it runs C++ and can do so very fast. Then we will do most of the computing on a Raspberry Pi because it has more power in that regard and we will have an easier time implementing more features on it.
* RPI 4 for the wifi capabilities, low power consumption, easily replaceable, more processing power than the arduino, allows for implementation of UI




### 2.(Simon)
Mimicking a joystick with an arduino
* Using the arduino as the controller part of our system while RPI would be responsible for the “Logic” part of the system which would set the boundaries for the actuator movement degree.
Choice of microcontroller
* The chosen microcontroller for our project is the arduino which has enough functionality to execute successfully the movement of the actuators which control the solar panel.
* The microcontroller also need to communicate with the sensor that keeps track of the angle
UI
* User interface would play a big role in our system as it would not require extra knowledge coming from the inexperienced user.
 


### 3.(Vladimir)
We will be building an automated system which would eliminate unnecessary user interaction, and will involve a solar panel which would always be pointed towards the sun to get the most energy out of solar waves.
Friendly user-interface, intuitive
* A good way to do it would be to have the UI mimic the physical controller with 8 degrees of motion, so the people who have used it before, will have an easy way to learn the UI controls. Also to give new users a visual representation of how the UI controls work.
* When doing automation we should be decreasing the risk of human error, meaning the code, parts etc. should be properly guarded against user errors.
* The solar tracking should be as fast as possible but real time isn’t necessary  






## Resources:
Brainstorming explained:
https://en.wikipedia.org/wiki/Brainstorming

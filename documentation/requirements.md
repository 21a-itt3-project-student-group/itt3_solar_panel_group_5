Functional requirements:
As a user I want to be able to control the mode of the solar panel between automatic and manual so that I am able to move it myself.
As a user I want to be able to stop the system instantly by hitting an emergency button so that I will be able to interrupt the running system in a special situation.
As a user I want to be able to manually control the solar panel by tilting it up, down, left and right so that I can position the solar panel in the desired position.
As a user I want the solar panel to have some safe boundaries which it will stay in at all times so that it cannot cause any damage to itself.


Non-functional requirements:
System must have a command line interface.
System must be user friendly.

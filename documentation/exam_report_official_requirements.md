The project report, which constitutes the written part of the examination, shall contain at a 
minimum, the following: 
 
● front page including title; 
● list of contents; 
● introduction, including presentation of the problem, problem statement and angles of 
approach; 
● background, theory, method and analysis, including a description of and justification for 
the choice of any empirical data,1 in response to the problem statement; 
● conclusion (remember that there must be a connection between the introduction and the 
conclusion; it must be possible in principle to understand these two sections without 
reading the background and analysis sections); 
● contextualization; 
● bibliography (including all sources referenced in the project); 
● annexes (include only annexes essential to the report).  


 The final examination project shall be at least 15 and at most 20 standard pages in length. For 
each additional student taking part in the final project, this shall be increased by at least 10 and 
at most 20 standard pages. 



Source:
https://www.ucl.dk/globalassets/01.-uddannelser/a.-videregaende-uddannelser/internationale-uddannelser/it-technology-ap/dokumenter/curriculum-2018-it-technology.pdf?nocache=1ab2fc23-0d86-4957-8aa7-425499c39d3d
# Risk analysis

## Potential problems that could jeopardize our project
case 1: Group’s limited knowledge of planetary movement 
<br>

case 2: Potential breakdown/damage of the used components
<br>

case 3: Adjusting to a smaller group after working in bigger groups
<br>

case 4: Unrealistic expectation of final product scope
<br>

## Probability and possible solutions
To indicate the level of probability a “0-5” range scale is applied,  where 0 represents the highly unlikely and 5 the very likely. Each case is evaluated by the group as a whole to determine the likelihood of the given case


### case 1
Probability for case 1: 2
<br>
solution:   
Using premade specialized tools for tracking planetary movements
   
### case 2     
Probability for case 2: 3
<br>
solution:   
Follow general safety rules when handling technical/electronic equipment
   
### case 3   
Probability for case 3: 1
<br>
solution:   
Careful distribution of workload, so that no single person gets overloaded

### case 4
Probability for case 4: 3
<br>
solutions:   
Show great care of the time frame before adding new features
Evaluate necessity of a feature before implementation



# Plan_for_usertest
<br>

## User Test- Usability Testing
Usability testing evaluates the degree to which the system can be used by specified users with
effectiveness, efficiency and satisfaction in a specified context of use- ISO 9241  Ergonomics of human-system interaction
It is a great method which allows you to improve the Usability of the Product. Usability refers to the ease with which a user can use a product in order to achieve his/her goal and how useful the Product is. A product which is high on usability makes it easy for user to accomplish his/her goals.
Usability testing helps the Design and Development Teams to identify Usability problems in the early stages. Identification of Usability Problems in the early stages allows the Design Team to fix the problems at much cheaper cost and in less amount of time without effecting the Launch Schedule.
<br>

## Why this usertest ?
forcing the user into specific scenarios for testing will not allow the developers to fully understand the level of safeguarding necessary for the finished product
Using the unmoderated usability testing will provide a more organic view of the problems the user might encounter. Upon problem discovery, it would help the development team to identify possible solutions.
<br>

## Plan for conducting test:
Basically we want to give our system to either our peers or members of the faculty from our education, they will be the most likely end users for our product.
First they will test to see if they can make it work, then we would also like to see if they can break it somehow to expose any faults in the system.
The tests will be under our supervision so that we will have the best chance of learning new ways to improve our system and shore up its flaws
<br>

## Target audience for test:
The ideal participant in the user test will be someone with little to no knowledge of solar panels, programming or any of the hardware components. meaning that for the test to be successful the product should be useful for a end user without prior knowledge of how to operate a solar panel
<br>

## User test expectations:
Upon completion of user testing our team expects to get the most valuable feedback which would help to improve the system until the point where simple interaction with the system would happen without any possible offenses. 
The conclusion of a single test for a single participant. does not conclude the overall usertest, the usertest phase will be conducted as a continuous loop until adequate information on the usability standards is reached.

<br>

## resources: 
https://www.yukti.io/10-most-important-user-testing-methods/ 

Milestones

POC phase, make something that functions (barely) 
Period: 01/09/2021 until 20/09/2021
Have a working POC of our system. Degree sensor - have it working with arduino nano and C++. Have code that can expand and contract the actuonix L16.

Issue templates and documentation framwork (Simon) - Done
Issue board setup/management (Vladimir) - Done
Brainstorm Document - Done
Blockdiagram - Done
Milestones (Casper) - Done
Use Case - Done
Use Case Diagram - Done
Risk Analysis - Done
Flowchart - Done

Create a MVP of Control system and sun tracker
Period: 21/09/2021 until 25/10/2021
This is the timeframe of the first trimester of the project, we are determained to finish a minimal viaple product that can detect when the solar panel turns too fare. We also want to be done with a control system that turns the panel, without the use of  buttons.
Being able to track the sun somehow is also something we would like to add at this point, but we are not sure if it is required at this point in time.

Exam 26. okt – 28. okt - Week 43
Period 26/10/2021 until 28/10/2021
This is the alocated time for the first part of project exam. We expect to have the report done by this point and all of the other required documents.

After first part of exam - Final adjustment
Period 29/10/2021 until 19/12/2021
Complete the missing parts if there are any. Move on towards the second part of the project which requires us to design and develop a graphical user interface for the solar panel movement and management.

Part 2 project exam - 20 dec. to 22 dec. - week 51
Period 20/12/2021 until 22/12/2021
This is the allocated time for the second part of the project exam. Everything should be checked and revised, polished and ready for delivery for our teachers and auditory.
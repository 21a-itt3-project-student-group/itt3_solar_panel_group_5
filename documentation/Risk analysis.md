﻿Risk analysis


Potential problems that could undermine our project
1. Group’s limited knowledge of planetary movement 
   1. Using premade specialized tools for tracking planetary movements
2. Potential breakdown/damage of the used components
   1. Follow general safety rules when handling technical/electronic equipment
3. Adjusting to a smaller group after working in bigger groups
   1. Careful distribution of workload, so that no single person gets overloaded
4. Unrealistic expectation of final product scope
   1. Show great care of the time frame before adding new features
   2. Evaluate necessity of a feature before implementation


Probability 0-5 where 0 is highly unlikely and 5 is very likely


Probability for 1. 2
                2. 2
                3. 1
                4. 3
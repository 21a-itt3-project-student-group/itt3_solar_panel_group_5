
Exercises_ww35
2021-09-07 08:13:12 +0000
2021, Sep 07    
Exercises for ww35
Exercise 1 - Project Planning

Will you make a project planning for your project which include:

    Deliverable Steps
    Deliverable Steps with time schedule
    Deliverables diagram

Exercise instructions

Each group has to create one solution (project planning) You can create it:

    Either on the blackboard
    Or by free hand
    Or by using any tool
    Then take a picture and upload it to itslearning

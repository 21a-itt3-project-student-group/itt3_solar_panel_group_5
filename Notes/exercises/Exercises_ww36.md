Exercises for ww36
Exercise 1 - identify Use Cases

How to identify Use Cases? you can ask questions:

	What functions will a specific actor want from the system?
Manual or automatic
best possible position for gathering solar panel
Control of the Panel via system rather than buttons
	Who gets support from the system to do the daily work?
DTB
	Who shall maintain, administrate the system? (Secondary actors)
Educators/teachers
	What hardware units are used by the system?
Some sensors to register if the panel moves to fare, so that it won’t be damaged
	What hardware units use the system?
A programmable system - either Aruduino or Raspberrypi (maybe even a microprocessor)
	What other systems do the system cooperate with?
a site which we scrape data from
maybe something powered by the solar panel
	Who or what has interest in the results generated of the system?
the Educators/teachers responsible for the solar panel
	Do the system store or retrieve information?
Yes, the system retrieves information from a function which calculates how the solar panel has to be positioned so that it get the most brightness out of the solar waves.
	Are any actors notified when the system change state?
No, but i could be set to message the actors if the weather conditions could damage the panel
	Are there any external events that affect the system?
weather and day/night cycle
	What notifies the system about those events?
the site we scrape information from 
	What are the main tasks performed by each of the actors?
the system should be automated, so mostly they should make sure that wifi is working and site is up and can be scraped from. 
also the actors have to make sure that the panel is protected from the weather and is not covered in dust/snow/dirt
	Will the Actor read or update any information in the system?
The actor will update the site in which the program gets its information and should be able to change the wifi information/password/network
	Will the Actor have to inform the system about changes outside the system?
yes: “The actor will update the site in which the program gets its information and should be able to change the wifi information/password/network”
	Does the Actor have to be informed about unexpected changes?
An LED will light if either the Wifi or the site is down

Exercise 2 - Use Cases Characteristic

Characteristic of the good specification:

	Is unambiguous/evident
Solar panel has to face the sun waves
	Is complete
Solar panel with an implemented manual moving mechanism
	Is consistent
It can run uninterruptedly
	Is correct
It can generate electricity by absorbing solar waves
	Tells what but not how
	It is necessary to make it always face the sun
	Can be understood of the end-user/customer
		At the end of developing this product, an automated solar panel will exist which will not require human interaction after the full set-up.
	Can be tested
Boundary limits of the moving solar panel
Correctness of the position of the solar panel
	Changeable
		The possibility of choosing between manual and automatic use of the solar panel
	Traceable
		The amount of electricity generated per a given amount of time

Exercise 3 - Use Case model

Use Case model step by step with your project

    	Draw the Actor-context diagram
        	a) Define the boundary of the system (what will be delivered)
    	Find the Actors and describe them
    	Find the Use Cases for each actor
        	a) If new actors comes up they should be added to the model
    	Draw the Use Case diagram
    	Write the Use Case specifications for the Use Cases one at a time
    	Exercise instructions

    	Exercise 3 - using Use case specification elements
	Actor
	Actor-context diagram
	Actor description
	Use Cases
	Use Case diagram
	Use Case descriptions
	Associations/Communication



Dp9 using serial communication<br>
H bridge<br>
One signal goes up another one goes down<br>

Exercise 0

	1. Risk analysis - Done
	2. Hardware/Prototype - Postponed until next lecture

Exercise 1<br>
RPI:<br>
	- Communication program:<br>
	- Receive / dataprocessor<br>
	- send commands for arduino<br>
	- Send confirmation that RPI is still alive<br>

Movement program:<br>
	- Boot sequence (which position am i in right now)<br>
	- manualMove - assign some keys to move the actuators manually<br>
	- automaticMove - movement along the sun where actuators move automatically<br>


detectBoundaries<br>
emergencyStop - hardwired, stop the power completely<br>


Arduino:<br>

I2c - big program<br>
	- Send ( all 3 positions at a time) <br>
	- receive<br>
	- Get angle data / maybe do some light processing of the data to make it understandable<br>
	- Turn pins high/low<br>
	- Make sure pin turns down at some point even without the command from RPI (for safety)<br>

